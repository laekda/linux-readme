#!/bin/bash

echo "Veuillez branchez votre périphérique de stockage"
echo "Laissez le branche jusqu'a la fin du processus"
echo
echo "Rentrer le nom de la distribution recherchee"
read build

if [ $build = "ubuntu" ]
then
	name="ubuntu-20.04-desktop-amd64.iso"
	url="http://releases.ubuntu.com/20.04/"$name
elif [ $build = "debian" ]
then
	name="debian-10.4.0-amd64-netinst.iso"
	url="https://saimei.ftp.acc.umu.se/debian-cd/current/amd64/iso-cd/"$name
elif [ $build = "mint" ]
then
	name="linuxmint-19.3-cinnamon-32bit.iso"
	url="http://ftp.crifo.org/mint-cd/stable/19.3/"$name
fi

echo "Downloading $url"
wget $url -O $name

sudo fdisk -l

echo "rentrez le peripherique recherche"
read cle

sudo dd if=$name of=/dev/$cle bs=4M status=progress && sync

rm $name
