#!/bin/bash

Actual_path=`pwd`
cd "$Actual_path/Documentation"
Liste=`ls`

for name in $Liste; do
    echo $name
    if [ -d $name ]; then
        cd $name
        if [ -f main.pdf ]; then
            cp "main.pdf" "$Actual_path/$name/doc.pdf"
            echo "transfert done"
        else
            echo "no file found"
        fi
        cd ..
    fi
done

cd "$Actual_path"
